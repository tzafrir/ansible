#!/bin/bash

# set -ex

### sudo apt install qemu qemu-utils qemu-system ovmf

# give user rw to usb device
### sudo adduser $USER disk
# (need to logout/in for this to take affect.)
# or do some other trick that I don't think will let the script run :p

# dev of usb stick
target=$1

# blank disk to install to
qemu-img create -f qcow2 disk.cow 8G

# run the installer
# (don't forget to run the server that hosts the preseed files)

# uefi
qemu-system-x86_64 -m 512 \
    -bios OVMF.fd \
    -drive file=disk.cow,index=0 \
    -drive file=${target},index=1,format=raw,if=none,id=thumb \
      -device ide-hd,drive=thumb,bootindex=1

# legacy
qemu-system-x86_64 -m 512 \
    -drive file=disk.cow,index=0 \
    -drive file=${target},index=1,format=raw,if=none,id=thumb \
      -device ide-hd,drive=thumb,bootindex=1

