# opsis

Configures a machine as an Opsis capture PC.

Also supports Black Magic Web Presenter devices, that present a similar
v4l USB device.

## Tasks

Tasks are separated in two different parts:

* `tasks/opsis.yml` manages the tools to setup up and connect to the opsis.

* `tasks/bmusb.yml` installs a udev rule to grant permissions to Blackmagic Web
   Presenter USB device.

* `tasks/hdmi2usbmond.yml` manages the HDMI2USB monitoring tools.

## Available variables

Main variables are:

* `serial_terminal`: List. Installs serial terminal packages that are in Debian.
                     The first one listed will be used for the `opsis` command
                     used to connect to the opsis board.

* `debian_version`:  Version of Debian, when using Debian.
